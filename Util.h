//
// Created by Filip Jani on 11.10.16.
//

#ifndef UPS_GUESSERINOSERVER_UTIL_H
#define UPS_GUESSERINOSERVER_UTIL_H

#include <string>
#include <vector>
#include <random>
#include <thread>

class Util {
public:

    /** Player structure */
    struct player_t {
        int socket;                 // players socket
        std::string nickName;       // players nickname
        int score;                  // players score
        bool isReady;               // is player ready
        bool listenerRunning;       // if listener for player is running
        bool hasDrawn;              // has player drawn
        bool isDrawing;             // is player drawing now
        bool hasGuessed;            // has guessed correct word
        bool isOnline;              // If player is online
        bool hasDisconnected;       // flag that player disconnected from server
        std::thread playerThread;   // players thread
    };

    /** Splits message by delimiter into std::vector */
    static std::vector<std::string> split(const std::string str, const std::string delim);

    /** Gets random word for drawing */
    static std::string getRandomWord();

    /** Gets random number from <0, size> */
    static int getRandomNumber(long size);

    /** Gets file content in String */
    static std::string get_file_content(std::string file_path);
};


#endif //UPS_GUESSERINOSERVER_UTIL_H
