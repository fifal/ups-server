//
// Created by Filip Jani on 22.10.16.
//

#ifndef UPS_GUESSERINOSERVER_CONNECTION_H
#define UPS_GUESSERINOSERVER_CONNECTION_H


#include "Logger.h"

class ServerConfiguration {
public:
    /** Struct for server config */
    struct configuration {
        std::string ipAddress;
        int serverSocket;
        int serverPort;
        int tcpQueue;
        int maxPlayersPerGame;
        int maxGames;
        int txtMaxLength;
        Logger *logger;
    } config;

    /** Constructor */
    ServerConfiguration(std::string ipAddress, int serverPort, int tcpQueue, int maxPlayers, int maxGames, int txtMaxLength, Logger *logger);
};


#endif //UPS_GUESSERINOSERVER_CONNECTION_H
