//
// Created by Filip Jani on 8.11.16.
//

#ifndef UPS_SERVER_TIMER_H
#define UPS_SERVER_TIMER_H

#include <time.h>

class Timer {
private:
    /** Structs for start time and finish time */
    struct timespec startTime, finishTime;
    /** Elapsed time */
    double elapsed;

public:
    /** Starts the timer */
    void start();
    /** Gets elapsed time */
    double elapsedTime();
};


#endif //UPS_SERVER_TIMER_H
