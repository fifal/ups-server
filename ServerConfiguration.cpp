//
// Created by Filip Jani on 22.10.16.
//
// Class where is server configuration stored
//

#include "ServerConfiguration.h"

/**
 * Constructor
 * @param serverPort port for the server
 * @param tcpQueue max number of clients in queue
 * @param maxPlayers max number of players in one game
 * @param maxGames  max number of games on server
 * @param txtMaxLength max length of message from client
 * @param logger server logger
 */
ServerConfiguration::ServerConfiguration(std::string ipAddress, int serverPort, int tcpQueue, int maxPlayers, int maxGames, int txtMaxLength, Logger *logger) {
    this->config.serverPort             = serverPort;
    this->config.tcpQueue               = tcpQueue;
    this->config.serverSocket           = -1;
    this->config.maxPlayersPerGame      = maxPlayers;
    this->config.maxGames               = maxGames;
    this->config.txtMaxLength           = txtMaxLength;
    this->config.logger                 = logger;
    this->config.ipAddress              = ipAddress;
}