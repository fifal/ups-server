//
// Created by Filip Jani on 24.10.16.
//
// Class providing functions for network stuff.
//      - sending/receiving messages
//      - checking if message is valid
//

#include <sys/socket.h>
#include <thread>
#include "Communication.h"
#include "string.h"

/**
 * Constructor
 * @param serverConfiguration configuration of server
 */
Communication::Communication(ServerConfiguration::configuration serverConfiguration) {
    this->serverConfiguration = serverConfiguration;
    m_messagesToSend = std::make_shared<std::vector<Message>>(std::move(std::vector<Message>()));

    std::thread messageThread = std::thread(messageSender, this);
    messageThread.detach();
}

/**
 * Receives message from client
 * @param socket client's socket
 * @return string with client's message
 */
std::string Communication::receiveMessage(int socket) {
    // Initialization of char array with max length defined in server configuration
    char message[this->serverConfiguration.txtMaxLength];
    // Setting all chars in array to 0
    memset(message, 0, this->serverConfiguration.txtMaxLength);

    // Reading message from client
    int result = (int) read(socket, &message, this->serverConfiguration.txtMaxLength - 1);

    // If result is less than zero -> error while getting message
    if (result < 0) {
        this->serverConfiguration.logger->error("s", "[receiveMessage]: Chyba při příjmání zprávy.");
        return Network::PROTOCOL_PREFIX + Network::getMessage(Network::SRV_CLIENT_DROPPED);
    }

        // If result equals to zero -> client dropped -> closes client's socket
    else if (result == 0) {
        close(socket);
        return Network::PROTOCOL_PREFIX + Network::getMessage(Network::SRV_CLIENT_DROPPED);
    }

        // Else message handling
    else {
        int i = 0;

        // While char at index i is not ';' add char into messageString
        while (message[i] != ';' && i < this->serverConfiguration.txtMaxLength && message[i] != '\0') {
            m_message += message[i];
            i++;
        }
        if(message[i] == ';'){
            m_message += message[i];
        }

        // Logs received message
        this->serverConfiguration.logger->debug("sssd", "[receiveMessage]: Přijata zpráva: ", m_message.c_str(),
                                                " délka je ",
                                                m_message.length());

        // Checks if message is valid, if is valid returns message, else returns NOT_VALID_MESSAGE
        if(strstr(m_message.c_str(), ";")) {
            if (isValidMessage(m_message)) {
                m_message.replace(m_message.find(";"), 1, "\0");
                std::string messageString = m_message;
                m_message = "";
                return messageString;
            } else {
                std::string message = Network::PROTOCOL_PREFIX + Network::getMessage(Network::NOT_VALID_MESSAGE);
                sendMessage(socket, message);
                return message;
            }
        }else{
            return receiveMessage(socket);
        }
    }

}

/**
 * Sends message to client
 * @param socket client's socket
 * @param message char*
 */
int Communication::sendMessage(int socket, std::string message) {
    const char *msgChar = message.c_str();
    int result = send(socket, (void *) msgChar, message.length(), 0);
    this->serverConfiguration.logger->debug("ss", "[sendMessage]: Posílám zprávu: ", message.replace(message.find("\n"), 1, "").c_str());
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
    return result;
}

/**
 * Returns true if message is valid
 * @param message
 * @return
 */

//TODO: ošetření platnosti zpráv - všech :(
bool Communication::isValidMessage(std::string message) {
    if(message.at(message.size()-1)==';') {
        message.replace(message.find(";"), 1, "\0");
    }
    std::vector<std::string> splittedMessage = Util::split(message, ":");

    if (splittedMessage.size() < 3 || splittedMessage.size() > 4) {
        return false;
    } else {
        if (splittedMessage[0].compare(Network::PROTOCOL_PREFIX_WO) == 0) {
            if (Network::getEnum(splittedMessage[1]) == Network::NOT_NETWORK_CODE) {
                return false;
            } else if(Network::getEnum(splittedMessage[1]) == Network::CL_GUESS && splittedMessage.size() == 4){
                return true;
            } else if(Network::getEnum(splittedMessage[1]) == Network::CL_CONNECT && splittedMessage.size() == 4){
                return true;
            }else if(Network::getEnum(splittedMessage[1]) == Network::CL_IMAGE && splittedMessage.size() == 4){
                return true;
            }else if(Network::getEnum(splittedMessage[1]) == Network::CL_DISCONNECT && splittedMessage.size() == 3){
                return true;
            }else if(Network::getEnum(splittedMessage[1]) == Network::CL_READY && splittedMessage.size() == 3){
                return true;
            }else if(Network::getEnum(splittedMessage[1]) == Network::CL_ACK && splittedMessage.size() == 3){
                return true;
            }
            else {
                return false;
            }
        } else {
            return false;
        }
    }
}

void Communication::messageSender(Communication *communication) {
    while (true) {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
        communication->messageMutex.lock();
        for (int i = 0; i < communication->m_messagesToSend.get()->size(); i++) {

            if (!communication->m_messagesToSend.get()->at(i).is_confirmed() && communication->m_messagesToSend.get()->at(i).get_current_time() > 5) {
                int result = communication->sendMessage(communication->m_messagesToSend.get()->at(i).get_socket(), communication->m_messagesToSend.get()->at(i).get_string());
                communication->m_messagesToSend.get()->at(i).update_sent_time();
                if(result == -1){
                    communication->m_messagesToSend.get()->at(i).confirm(true);
                }
            } else if(communication->m_messagesToSend.get()->at(i).is_confirmed()){
                communication->m_messagesToSend.get()->erase(communication->m_messagesToSend.get()->begin() + i);
            }
        }
        communication->messageMutex.unlock();
    }
}

void Communication::addMessage(Message message) {
    m_messagesToSend.get()->push_back(message);
    sendMessage(message.get_socket(), message.get_string());
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
}

void Communication::ackMessage(size_t hashCode) {
    std::lock_guard<std::mutex> lk(messageMutex);
    for (int i = 0; i < m_messagesToSend.get()->size(); ++i) {
        if (m_messagesToSend.get()->at(i).get_hash_code() == hashCode) {
            m_messagesToSend.get()->at(i).confirm(true);
            serverConfiguration.logger->debug("ss", "[ackMessage]: Potvrzeno ", std::to_string(m_messagesToSend.get()->at(i).get_hash_code()).c_str());
        }
    }
}
