//
// Created by fifal on 8.1.17.
//

#include <algorithm>
#include <sstream>
#include "GameRoom.h"
#include "Network.h"

/**
 * Game Room constructor
 *
 * @param serverConfig
 * @param gameRoomId
 */
GameRoom::GameRoom(ServerConfiguration::configuration serverConfig, int gameRoomId, Communication * communication) {
    m_serverConfig = serverConfig; /* Settings of server */
    m_communication = communication; /* Class for TCP stuff */

    // GameRoom initialization
    m_ID = gameRoomId; /* ID of this room */
    m_players = std::vector<std::shared_ptr<Util::player_t>>(); /* Vector for players */
    m_img = "";

    // GameRoom Loop initialization
    m_gameState = WAITING; /* Current state of game */
    m_drawTime_s = 120; /* Max time for drawing */
    m_guessTime_s = 60; /* Max time for guessing */
    m_reconnectTime_s = 10; /* Max time for waiting for player to reconnect */

    // GameRoom Loop thread
    m_gameRoomThread = std::thread(gameRoomLoop, this);
    m_gameRoomThread.detach();
}

/**
 * Listener for one player
 * @param player
 * @param gr
 */
void GameRoom::playerListener(int socket, GameRoom *gr) {
    std::shared_ptr<Util::player_t> player = gr->getPlayerBySocket(socket);

    if (player == nullptr) {
        gr->m_serverConfig.logger->debug("sdsd", "[ROOM ", gr->m_ID,
                                         "][playerListener]: Chyba při získávání údajů pro hráče se socketem ",
                                         socket);
        //TODO: Ošetřit
        return;
    }
    gr->m_serverConfig.logger->info("sdsdss", "[ROOM ", gr->m_ID, "][playerListener]: Místnost ", gr->m_ID,
                                    " spustila listener pro hráče ",
                                    player.get()->nickName.c_str());

    while (player.get()->listenerRunning) {
        std::string message = gr->m_communication->receiveMessage(socket);
        std::vector<std::string> splitMessage = Util::split(message, ":");

        if (Network::getEnum(splitMessage[1]) != Network::CL_ACK
            && Network::getEnum(splitMessage[1])!= Network::SRV_CLIENT_DROPPED
            && Network::getEnum(splitMessage[1])!= Network::NOT_VALID_MESSAGE) {

            std::string ack_str = Network::PROTOCOL_PREFIX + Network::getMessage(Network::SRV_ACK) + ":" +
                                  splitMessage[splitMessage.size() - 1] + Network::PROTOCOL_SUFFIX;
            gr->m_communication->sendMessage(socket, ack_str);
        }

        // Action based on network message code
        switch (Network::getEnum(splitMessage[1])) {
            // User is ready
            case Network::CL_READY: {
                gr->m_serverConfig.logger->info("sdsss", "[ROOM ", gr->m_ID, "][playerListener]: Hráč ",
                                                player.get()->nickName.c_str(),
                                                " je ready.");
                gr->setReady(socket, true);
                gr->sendInfo("<>Hráč " + player.get()->nickName + " je ready.<>");
                break;
            }

                // User guessed word
            case Network::CL_GUESS: {
                if (gr->m_gameState == GUESSING && !player.get()->hasGuessed) {
                    // Get players response to lower
                    std::string guessedWord = splitMessage[2];
                    std::transform(guessedWord.begin(), guessedWord.end(), guessedWord.begin(), ::tolower);

                    // Get guessing word to lower
                    std::string word = gr->m_guessWord;
                    std::transform(word.begin(), word.end(), word.begin(), ::tolower);

                    // If guessed correct word
                    if (guessedWord.compare(word) == 0) {
                        gr->m_serverConfig.logger->info("sdsss", "[ROOM ", gr->m_ID, "][playerListener]: Hráč ",
                                                        player.get()->nickName.c_str(),
                                                        " uhodl hádané slovo.");
                        // Increase players score and decrease max score for current level by one
                        gr->increasePlayerScore(socket, gr->m_scoreMax);
                        gr->sendInfo("<>## Hráč "
                                     + player.get()->nickName
                                     + " uhodl hádané slovo a získal "
                                     + std::to_string(gr->m_scoreMax) + " bodů.##<>");
                        gr->m_scoreMax--;

                        gr->setPlayerGuessed(socket, true);

                        std::string score_msg = Network::PROTOCOL_PREFIX
                                                + Network::getMessage(Network::SRV_SCORE)
                                                + std::to_string(player.get()->score)
                                                + Network::PROTOCOL_SUFFIX;
                        Message msg(player.get()->socket, std::to_string(player.get()->score), Network::SRV_SCORE);
                        gr->m_communication->addMessage(msg);
                    } else {
                        gr->sendInfo(
                                "<>## Hráč " + player.get()->nickName + " hádal slovo \"" + splitMessage[2] +
                                "\", ale neuhodl.##<>");
                    }
                }
                break;
            }

                // User sent image
            case Network::CL_IMAGE: {
                if (gr->m_gameState == WAITING_FOR_DRAW && player.get()->isDrawing) {
                    gr->m_serverConfig.logger->info("sdsss", "[ROOM ", gr->m_ID,
                                                    "][playerListener]: Přijat obrázek od hráče ",
                                                    player.get()->nickName.c_str(), ".");

                    gr->m_img = splitMessage[2];

                    gr->sendImage(player.get()->socket);

                    // Set player details
                    gr->setPlayerDrawing(socket, false);
                    gr->setPlayerDrew(socket, true);

                    // Set max score to size of players vector
                    gr->m_scoreMax = gr->m_players.size();
                }
                break;
            }

                // Connection with client was lost
            case Network::SRV_CLIENT_DROPPED: {
                gr->m_serverConfig.logger->info("sdsss", "[ROOM ", gr->m_ID, "][playerListener]: Spojení s hráčem \"",
                                                player.get()->nickName.c_str(),
                                                "\" ztraceno.");
                gr->sendInfo("<>Spojení s hráčem " + player.get()->nickName + " ztraceno.<>");

                // Set player flag isOnline to false
                gr->setPlayerOnline(socket, false);
                gr->setPlayerListenerRunning(socket, false);

                break;
            }

            case Network::CL_ACK: {
                std::string hashStr = splitMessage.at(2);
                std::stringstream sstream(hashStr);
                size_t hash;
                sstream >> hash;
                gr->m_communication->ackMessage(hash);
                break;
            }

                // Network code is not allowed
            default: {
                break;
            }
        }
    }

    gr->waitForPlayerReconnect(socket);

    gr->m_serverConfig.logger->debug("sdsss", "[ROOM ", gr->m_ID, "][playerListener]: Končím vlákno pro hráče \"",
                                     player.get()->nickName.c_str(), "\".");
}

/**
 * Game Room Loop
 * @param gr
 */
void GameRoom::gameRoomLoop(GameRoom *gr) {
    gr->m_serverConfig.logger->info("sdsds", "[ROOM ", gr->m_ID, "][gameRoomLoop]: Místnost ", gr->m_ID,
                                    " spustila listener pro game loop.");
    Timer timer;

    while (true) {
        gr->sleep(1);
        int drawing_player_socket = gr->getNextDrawingPlayer();
        std::shared_ptr<Util::player_t> drawing_player = gr->getPlayerBySocket(drawing_player_socket);
        // State 1 (WAITING for everyone ready)
        if (gr->isEveryoneReady() && drawing_player_socket != -1 && gr->m_gameState == WAITING) {

            // State 2 (WAITING_FOR_DRAW one of players is drawing)
            gr->setPlayerDrawing(drawing_player_socket, true);
            gr->m_drawingPlayerName = drawing_player.get()->nickName;

            gr->m_guessWord = Util::getRandomWord();

            // While client didn't ack receiving word -> resend
            gr->sendWord(drawing_player.get()->socket);
            gr->setButtons(drawing_player.get()->socket);

            gr->sendInfo("<>## Hráč " + drawing_player->nickName + " kreslí.##<>");

            // Set Game State
            gr->m_gameState = WAITING_FOR_DRAW;

            gr->checkForPlayers();

            // Wait for drawing
            bool player_drew = false;
            int sentAt = 0;
            timer.start();
            while (timer.elapsedTime() < gr->m_drawTime_s) {
                gr->sleep(1);
                double elapsedTime = timer.elapsedTime();

                if (gr->getHasPlayerDrawn(drawing_player.get()->nickName)) {
                    player_drew = true;
                    break;
                }

                if (((int) elapsedTime % 5) == 0) {
                    int timeLeft = (int) gr->m_drawTime_s - (int) elapsedTime;
                    if (timeLeft != sentAt) {
                        gr->sendInfo("Zbývá " + std::to_string(timeLeft) + "s na kreslení.");
                        sentAt = timeLeft;
                    }
                }

                if (!gr->isEnoughPlayers()) {
                    gr->sendInfo("<>V místnosti zbylo málo hráčů pro pokračování, končím hru!!!<>");
                    gr->m_gameState = ENDING;
                    break;
                }
            }

            gr->checkForPlayers();

            // If player drew go to game state guessing else -> go back to state 2
            while (player_drew && gr->isEnoughPlayers()) {
                gr->sleep(1);
                gr->m_serverConfig.logger->info("sdsss", "[ROOM ", gr->m_ID, "][gameRoomLoop]: Hráč ",
                                                drawing_player.get()->nickName.c_str(),
                                                " nakreslil obrázek.");

                gr->sendInfo("<> ## Hráč " + drawing_player.get()->nickName + " nakreslil obrázek.##<>");

                gr->checkForPlayers();

                // Set game state
                gr->m_gameState = GUESSING;

                // Start timer for guessing
                gr->m_serverConfig.logger->info("sdss", "[ROOM ", gr->m_ID,
                                                "][gameRoomLoop]: Spouštim timer na hádání s=",
                                                std::to_string(gr->m_guessTime_s).c_str());


                sentAt = 0;
                timer.start();
                while (timer.elapsedTime() < gr->m_guessTime_s) {
                    gr->sleep(1);
                    double elapsedTime = timer.elapsedTime();
                    gr->checkForPlayers();

                    if (((int) elapsedTime % 5) == 0) {
                        int timeLeft = (int) gr->m_guessTime_s - (int) elapsedTime;
                        if (timeLeft != sentAt) {
                            gr->sendInfo("Zbývá " + std::to_string(timeLeft) + "s na hádání.");
                            sentAt = timeLeft;
                        }
                    }

                    if (gr->hasEveryoneGuessed(drawing_player.get()->socket)) {
                        gr->sleep(1000);
                        break;
                    }

                    if (!gr->isEnoughPlayers()) {
                        gr->sendInfo("<>V místnosti nezbyl dostatek hráčů pro pokračování, končím hru!!!<>");
                        break;
                    }
                }
                break;
            }
            if (!player_drew) {
                gr->sendInfo("<>## Hráč " + drawing_player.get()->nickName +
                                     " nenakreslil obrázek v časovém limitu!##<>");
                gr->setPlayerDrew(drawing_player.get()->socket, true);

            }

            if (gr->isEnoughPlayers() && gr->getNextDrawingPlayer() != -1) {
                gr->m_gameState = WAITING;
            } else {
                gr->m_gameState = ENDING;
            }

            // Reset acknowledgements from this round
            //gr->resetPlayersAck();
            gr->m_img = "";

        } else if (gr->m_gameState == ENDING) {
            gr->sendResultsAndResetRoom();

            gr->m_gameState = WAITING;
        }
    }
}

/**
 * Adds player into room
 * @param player
 */
void GameRoom::addPlayer(Util::player_t player) {
    m_serverConfig.logger->info("sdsssds", "[ROOM ", m_ID, "][addPlayer]: Přidávám hráče ", player.nickName.c_str(),
                                " do místnosti ",
                                m_ID, ".");
    std::string nick = player.nickName;

    std::lock_guard<std::mutex> lk(m_playersMutex);
    if (m_players.size() != m_serverConfig.maxPlayersPerGame) {
        player.hasDrawn = false;
        player.hasGuessed = false;
        player.isDrawing = false;
        player.isOnline = true;
        player.listenerRunning = true;
        player.isReady = false;
        player.score = 0;
        player.hasDisconnected = false;

        player.playerThread = std::thread(playerListener, player.socket, this);
        player.playerThread.detach();

        m_players.push_back(std::make_shared<Util::player_t>(std::move(player)));

        m_serverConfig.logger->info("sdsssds", "[ROOM ", m_ID, "][addPlayer]: Hráč ", player.nickName.c_str(),
                                    " byl přidán do místnosti ",
                                    m_ID, ".");
        sendInfo("<>Hráč " + nick + " se připojil do hry.<>");
    }
}

/**
 * Removes player from room
 * @param socket players socket
 */
void GameRoom::removePlayer(int socket) {
    int playerIndex = getPlayerIndex(socket);
    if (playerIndex == -1) {
        m_serverConfig.logger->info("sdsds", "[ROOM ", m_ID, "][removePlayer]: Chyba, hráč se socketem ", socket,
                                    " nenalezen!");
    } else {
        m_serverConfig.logger->info("sdsds", "[ROOM ", m_ID, "][removePlayer]: Odebírám hráče se socketem ", socket,
                                    ".");

        Message msg = Message(socket, "", Network::SRV_DISCONNECT);
        m_communication->addMessage(msg);
        shutdown(socket, SHUT_RDWR);
        close(socket);
        setPlayerListenerRunning(socket, false);

        std::lock_guard<std::mutex> lk(m_playersMutex);
        m_players.erase(m_players.begin() + playerIndex);
        m_serverConfig.logger->info("sdsds", "[ROOM ", m_ID, "][removePlayer]: Hráč se socketem ", socket,
                                    " byl odstraněn!");
    }
}

/**
 * Reconnects player into the room
 * @param player
 */
void GameRoom::reconnectPlayer(Util::player_t player) {
    std::shared_ptr<Util::player_t> disconnected_player = getPlayerByName(player.nickName);
    if (isLoggedButNotOnline(player.nickName) && disconnected_player != nullptr) {
        m_serverConfig.logger->info("sdsss", "[ROOM ", m_ID, "][reconnectPlayer]: Připojuji hráče ",
                                    player.nickName.c_str(), ".");

        setPlayerListenerRunning(disconnected_player.get()->socket, true);

        disconnected_player.get()->socket = player.socket;
        disconnected_player.get()->playerThread = std::thread(playerListener, player.socket, this);
        disconnected_player.get()->playerThread.detach();

        std::string connected_message = Network::PROTOCOL_PREFIX
                                        + Network::getMessage(Network::SRV_SUCCESSFULLY_LOGGED);
        Message msg(player.socket, "", Network::SRV_SUCCESSFULLY_LOGGED);
        m_communication->addMessage(msg);

        Message msg_score(player.socket, std::to_string(disconnected_player.get()->score), Network::SRV_SCORE);
        m_communication->addMessage(msg_score);

        setPlayerOnline(player.socket, true);


        if (m_drawingPlayerName == player.nickName) {
            Message msg_word(player.socket, "", Network::SRV_DRAW);
            m_communication->addMessage(msg_word);

            sendWord(player.socket);
        } else {
            Message msg_btns(player.socket, "", Network::SRV_BTNS_GUESS);
            m_communication->addMessage(msg_btns);

            if (m_gameState != WAITING_FOR_DRAW && m_gameState != ENDING && m_img != "") {
                Message msg(player.socket, m_img, Network::SRV_IMAGE);
                m_communication->addMessage(msg);
            }
        }

        sendInfo("<>Hráč " + player.nickName + " se připojil zpět do hry.<>");

        m_serverConfig.logger->info("sdsss", "[ROOM ", m_ID, "][reconnectPlayer]: Hráč ", player.nickName.c_str(),
                                    " byl připojen.");
    }
}
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//                          PLAYERS HANDLING STUFF                           -
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
/**
 * Sets value in players struct - isReady
 * @param socket players socket
 * @param isReady bool
 */
void GameRoom::setReady(int socket, bool isReady) {
    std::shared_ptr<Util::player_t> player = getPlayerBySocket(socket);
    if (player == nullptr) {
        //TODO: Ošetřit
        return;
    }
    std::lock_guard<std::mutex> lk(m_playersMutex);
    player.get()->isReady = isReady;
}

/**
 * Sets value in players struct - isDrawing
 * @param socket players socket
 * @param isDrawing bool
 */
void GameRoom::setPlayerDrawing(int socket, bool isDrawing) {
    std::shared_ptr<Util::player_t> player = getPlayerBySocket(socket);
    if (player == nullptr) {
        //TODO: Ošetřit
        return;
    }
    std::lock_guard<std::mutex> lk(m_playersMutex);
    player.get()->isDrawing = isDrawing;
}

/**
 * Sets value in players struct - hasDrawn
 * @param socket players socket
 * @param hasDrawn bool
 */
void GameRoom::setPlayerDrew(int socket, bool hasDrawn) {
    std::shared_ptr<Util::player_t> player = getPlayerBySocket(socket);
    if (player == nullptr) {
        //TODO: Ošetřit
        return;
    }
    std::lock_guard<std::mutex> lk(m_playersMutex);
    player.get()->hasDrawn = hasDrawn;
}

/**
 * Checks if player has already drawn
 * @param socket player's socket
 * @return true if player has drawn, false otherwise
 *
 */
bool GameRoom::getHasPlayerDrawn(int socket) {
    std::shared_ptr<Util::player_t> player = getPlayerBySocket(socket);
    if (player != nullptr) {
        return player.get()->hasDrawn;
    }
}

/**
 * Checks if player has already drawn
 * @param nickName player's nick
 * @return true if player has drawn, false otherwise
 *
 */
bool GameRoom::getHasPlayerDrawn(std::string nickName) {
    std::shared_ptr<Util::player_t> player = getPlayerByName(nickName);
    if (player != nullptr) {
        return player.get()->hasDrawn;
    }
}

/**
 * Increases players score by increment
 * @param socket players socket
 * @param increment int
 */
void GameRoom::increasePlayerScore(int socket, int increment) {
    std::shared_ptr<Util::player_t> player = getPlayerBySocket(socket);
    if (player == nullptr) {
        //TODO: Ošetřit
        return;
    }
    std::lock_guard<std::mutex> lk(m_playersMutex);
    player.get()->score += increment;
}

/**
 * Sets value in players struct - hasGuessed
 * @param socket players socket
 * @param hasGuessed bool
 */
void GameRoom::setPlayerGuessed(int socket, bool hasGuessed) {
    std::shared_ptr<Util::player_t> player = getPlayerBySocket(socket);
    if (player == nullptr) {
        //TODO: Ošetřit
        return;
    }
    std::lock_guard<std::mutex> lk(m_playersMutex);
    player.get()->hasGuessed = hasGuessed;
}

/**
 * Sets value in players struct - isOnline
 * @param socket players socket
 * @param isOnline bool
 */
void GameRoom::setPlayerOnline(int socket, bool isOnline) {
    std::shared_ptr<Util::player_t> player = getPlayerBySocket(socket);
    if (player == nullptr) {
        //TODO: Ošetřit
        return;
    }
    std::lock_guard<std::mutex> lk(m_playersMutex);
    player.get()->isOnline = isOnline;
}

/**
 * Returns isOnline from player structure
 * @param socket players socket
 */
bool GameRoom::getPlayerOnline(int socket) {
    std::shared_ptr<Util::player_t> player = getPlayerBySocket(socket);
    if (player == nullptr) {
        //TODO: ????
        return false;
    }

    return player.get()->isOnline;
}

/**
 * Sets value in players struct - listenerRunning
 * @param socket players socket
 * @param isRunning bool
 */
void GameRoom::setPlayerListenerRunning(int socket, bool isRunning) {
    std::shared_ptr<Util::player_t> player = getPlayerBySocket(socket);
    if (player == nullptr) {
        //TODO: Ošetřit
        return;
    }
    std::lock_guard<std::mutex> lk(m_playersMutex);

    player.get()->listenerRunning = isRunning;
}

/**
 * Returns index of player in m_players vector
 * @param player
 * @return
 */
int GameRoom::getPlayerIndex(int socket) {
    std::lock_guard<std::mutex> lk(m_playersMutex);

    for (int i = 0; i < m_players.size(); ++i) {
        if (m_players.at(i).get()->socket == socket) {
            return i;
        }
    }
    return -1;
}

/**
 * Returns player struct by players socket
 * @param socket players socket
 * @return struct if successful, nullptr otherwise
 */
std::shared_ptr<Util::player_t> GameRoom::getPlayerBySocket(int socket) {
    std::lock_guard<std::mutex> lk(m_playersMutex);
    std::shared_ptr<Util::player_t> player = nullptr;

    for (int i = 0; i < m_players.size(); ++i) {
        if (m_players.at(i).get()->socket == socket) {
            player = m_players.at(i);
        }
    }

    return player;
}

/**
 * Returns player struct by player nickname
 * @param nickname string
 * @return struct if sucessful, nullptr otherwise
 */
std::shared_ptr<Util::player_t> GameRoom::getPlayerByName(std::string nickName) {
    std::lock_guard<std::mutex> lk(m_playersMutex);
    std::shared_ptr<Util::player_t> player = nullptr;

    for (int i = 0; i < m_players.size(); ++i) {
        if (m_players.at(i).get()->nickName == nickName) {
            player = m_players.at(i);
        }
    }

    return player;
}
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//                             Communication                                 -
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
/**
 * Sends info to all players which is displayed in client textarea
 * @param infoMessage info message
 */
void GameRoom::sendInfo(std::string infoMessage) {
    std::string message = Network::PROTOCOL_PREFIX
                          + Network::getMessage(Network::SRV_INFO)
                          + infoMessage
                          + Network::PROTOCOL_SUFFIX;

    for (int i = 0; i < m_players.size(); ++i) {
        Message message(m_players.at(i).get()->socket, infoMessage, Network::SRV_INFO);
        m_communication->addMessage(message);
    }
}

/**
 * Sets buttons to all players, drawing buttons to player with socket drawing_player_socket
 * @param drawing_player_socket int
 */
void GameRoom::setButtons(int drawing_player_socket) {
    Message drawing_msg(drawing_player_socket, "", Network::SRV_DRAW);
    m_communication->addMessage(drawing_msg);

    std::lock_guard<std::mutex> lk(m_playersMutex);
    for (int i = 0; i < m_players.size(); ++i) {
        if (m_players.at(i).get()->socket != drawing_player_socket) {
            Message guessing_msg(m_players.at(i).get()->socket, "", Network::SRV_BTNS_GUESS);
            m_communication->addMessage(guessing_msg);
        }
    }
}

/**
 * Sends word which player has to draw
 * @param drawing_player_socket drawing player socket
 */
void GameRoom::sendWord(int drawing_player_socket) {
    Message msg(drawing_player_socket, m_guessWord, Network::SRV_WORD);
    m_communication->addMessage(msg);
    sleep(50);
}

/**
 * Sends image to all players who didn't ack receiving img
 */
void GameRoom::sendImage(int drawing_player_socket) {
    std::lock_guard<std::mutex> lk(m_playersMutex);
    for (int i = 0; i < m_players.size(); ++i) {
        if (m_players.at(i).get()->socket != drawing_player_socket) {
            Message msg(m_players.at(i).get()->socket, m_img, Network::SRV_IMAGE);
            m_communication->addMessage(msg);
        }
    }
}
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//                             Game Room Loop                                -
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
/**
 * Checks if everyone in the game room is ready
 * @return true if yes, false otherwise
 */
bool GameRoom::isEveryoneReady() {
    // If there is enough players
    if (m_players.size() == m_serverConfig.maxPlayersPerGame) {
        std::lock_guard<std::mutex> lk(m_playersMutex);
        for (int i = 0; i < m_players.size(); ++i) {
            if (!m_players.at(i).get()->isReady) {
                return false;
            }
        }

        return true;
    }

    return false;
}

/**
 * Checks if everyone is online
 * @return true if yes, false otherwise
 */
int GameRoom::isEveryoneOnline() {
    for (int i = 0; i < m_players.size(); ++i) {
        if (!getPlayerOnline(m_players.at(i).get()->socket)) {
            return m_players.at(i).get()->socket;
        }
    }

    return 0;
}

/**
 * Checks if there is enough players in a room (min = 2)
 * @return true if yes, false otherwise
 */
bool GameRoom::isEnoughPlayers() {
    std::lock_guard<std::mutex> lk(m_playersMutex);

    for (int i = 0; i < m_players.size(); ++i) {
        if (m_players.at(i).get()->hasDisconnected) {
            return false;
        }
    }
    return true;
}

/**
 * Sends results to players and reset game settings
 *      -because we can't send ":" or "\n" in our message we have to replace them
 *          "<>" is set in client to "\n"
 *          "><" is set in client to ":"
 */
void GameRoom::sendResultsAndResetRoom() {
    m_serverConfig.logger->info("sds", "[ROOM ", m_ID, "][sendResultsAndReset]: Hra v mítnosti dokončena!");

    Timer timer;
    double disconnectTime = 10;

    // String with results
    std::string scoreMsg = "<><>## Výsledky hry ##<>";

    // Max score in current game
    int maxScore = -1;
    std::vector<std::string> winners;

    // Searching for player with biggest score
    for (int j = 0; j < m_players.size(); ++j) {
        if (m_players.at(j).get()->score > maxScore) {
            maxScore = m_players.at(j).get()->score;
        }
    }

    // Getting all players with max score in case of tie
    for (int k = 0; k < m_players.size(); ++k) {
        if (m_players.at(k).get()->score == maxScore) {
            winners.push_back(m_players.at(k).get()->nickName);
        }
    }

    if (winners.size() == 1) {
        scoreMsg += "Vítěz>< " + winners.at(0) + "! <><>Celkové výsledky><<>";
    } else {
        scoreMsg += "Remíza mezi hráči>< ";
        for (int i = 0; i < winners.size(); ++i) {
            if (i != 0) {
                scoreMsg += ", " + winners.at(i);
            } else {
                scoreMsg += winners.at(i);
            }
        }
        scoreMsg += "<><>Celkové výsledky><<>";
    }

    // Adding all results to message
    for (int i = 0; i < m_players.size(); ++i) {
        scoreMsg += "\t" + m_players.at(i).get()->nickName + ">< " +
                    std::to_string(m_players.at(i).get()->score) +
                    "<>";
    }

    // Send message to all players
    sendInfo(scoreMsg);

    // Log that timer for disconnect started
    m_serverConfig.logger->info("sdss", "[ROOM ", m_ID, "][sendResultsAndReset]: Spouštím timer na odpojení hráčů s=",
                                std::to_string((int) disconnectTime).c_str());

    // Send info to all players that they will be disconnected
    sendInfo("Po " + std::to_string((int) disconnectTime) + " sekundách budete odpojeni.");

    // Start timer
    timer.start();
    while (timer.elapsedTime() < disconnectTime) {
        sleep(1);
    }

    // Disconnect all players
    this->disconnectPlayers();
}

/**
 * Disconnects players in current room after game was finished
 */
void GameRoom::disconnectPlayers() {
    while (m_players.size() != 0) {
        removePlayer(m_players.at(0).get()->socket);
    }
}

/**
 * If all players ar not online wait for those who aren't
 */
void GameRoom::checkForPlayers() {
    int not_online = isEveryoneOnline();
    if (not_online != 0 && m_gameState != ENDING) {
        m_serverConfig.logger->info("sdss", "[ROOM ", m_ID, "][checkForPlayers]: Čekám na připojení hráče ",
                                    getPlayerBySocket(not_online).get()->nickName.c_str());
        waitForPlayerReconnect(not_online);
    }
}

/**
 * Waits for given time
 * @param time
 * @return true if waited for given time, returns false otherwise
 */
bool GameRoom::wait(double time) {
    Timer timer;
    timer.start();
    while (timer.elapsedTime() < time) {}

    return true;
}

/**
 * Sleeps current thread for milis
 * @param milliseconds int
 */
void GameRoom::sleep(int milliseconds) {
    std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds));
}

/**
 * Gets next drawing player socket
 * @return next drawing player's socket, -1 otherwise
 */
int GameRoom::getNextDrawingPlayer() {
    std::lock_guard<std::mutex> lk(m_playersMutex);

    for (int i = 0; i < m_players.size(); ++i) {
        if (!m_players.at(i).get()->hasDrawn) {
            return m_players.at(i).get()->socket;
        }
    }

    return -1;
}

/**
 * If player has disconnected wait for him for a bit
 * @param socket players socket
 */
void GameRoom::waitForPlayerReconnect(int socket) {
    std::shared_ptr<Util::player_t> disconnected_player = getPlayerBySocket(socket);
    if (disconnected_player == nullptr) {
        //TODO:????
        return;
    }

    m_serverConfig.logger->debug("s", "[waitForPlayerReconnect]: Čekám na připojení hráče.");
    Timer timer;
    timer.start();
    while (timer.elapsedTime() < m_reconnectTime_s) {
        sleep(1);
        if (disconnected_player.get()->isOnline) {
            break;
        }
        if (m_gameState == ENDING) {
            break;
        }
    }
    m_serverConfig.logger->debug("s", "[waitForPlayerReconnect]: Konec čekání na připojení hráče.");

    if (!getPlayerOnline(disconnected_player.get()->socket)) {
        sendInfo("<>Hráč " + disconnected_player.get()->nickName + " se nepřipojil v časovém limitu!<>");
        disconnected_player.get()->hasDisconnected = true;

        if (m_gameState == WAITING) {
            removePlayer(socket);
        }
    }
}
/**
 *
 * @param drawing_player_socket
 * @return
 */
bool GameRoom::hasEveryoneGuessed(int drawing_player_socket) {
    std::lock_guard<std::mutex> lk(m_playersMutex);
    for (int i = 0; i < m_players.size(); ++i) {
        if (m_players.at(i).get()->socket != drawing_player_socket && !m_players.at(i).get()->hasGuessed) {
            return false;
        }
    }
    return true;
}
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//                                BOOL GETTERS                               -
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
/**
 * Returns if room has free space
 * @return true if has free space, false otherwise
 */
bool GameRoom::isRoomFree() {
    return m_players.size() < m_serverConfig.maxPlayersPerGame && m_gameState != ENDING;
}

/**
 * Returns if nickname is available
 * @param nickName string
 * @return true if none of players has nick == nickName
 */
bool GameRoom::isNickAvailable(std::string nickName) {
    std::shared_ptr<Util::player_t> player = getPlayerByName(nickName);

    return player == nullptr ? true : false;
}

/**
 * Returns if player is logged in room but is not online
 * @param nickName players nick
 *@return true if logged but not online, false otherwise
 */
bool GameRoom::isLoggedButNotOnline(std::string nickName) {
    std::shared_ptr<Util::player_t> player = getPlayerByName(nickName);

    if (player == nullptr) {
        return false;
    }

    if (player.get()->isOnline == false && m_gameState != ENDING) {
        return true;
    }

    return false;
}

//----------------------------------------------------------------------------
GameRoom::~GameRoom() {

}
