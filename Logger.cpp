//
// Created by Filip Jani on 24.10.16.
//
// Class providing basic logger functions
//      - logs to a file + console (logging to console can be disabled in constructor)
//

#include <iostream>
#include "Logger.h"
#include "Debug.h"

/**
 * Constructor
 * @param filename filename where logger will log
 * @param _console if set to true logs will display in console aswell
 */
Logger::Logger(std::string filename, bool _console) {
    this->console = _console;
    this->logFile = filename;
    this->logStream.open(logFile.c_str(), std::ios::out | std::ios::app);
}

/**
 * Gets current system time
 * @return string of current system time
 */
std::string Logger::getTime() {
    time_t rawtime;
    struct tm *timeinfo;
    char buffer[80];

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer, 80, "%d-%m-%Y %H:%M:%S", timeinfo);
    std::string str(buffer);
    return str;
}

/**
 * Logs message into file and console
 * @param message string to log
 */
void Logger::log(std::string message, int type) {
    std::lock_guard<std::mutex> lk(logMutex);
    std::string logMessage = this->getTime() + ":" + message;
    if (this->console) {
        switch (type){
            case 0: {
                std::cout << "\033[0;32m" << logMessage << "\033[0m" << std::endl;
                break;
            }
            case 1:{
                std::cout << "\033[0;31m" << logMessage << "\033[0m" << std::endl;
                break;
            }
            case 2:{
                std::cout << "\033[0;34m" << logMessage << "\033[0m" << std::endl;
                break;
            }
        }
    }
    this->logStream << logMessage << std::endl;
    this->logStream.flush();
}

/**
 * Logs event of type info [INFO]
 * @param fmt char* of types "s" for string, "d" for int and "f" for double
 */
void Logger::info(char *fmt, ...) {
    std::string finalMessage = "";
    va_list args;
    va_start(args, fmt);
    while (*fmt != '\0') {
        if (*fmt == 'd') {
            int i = va_arg(args, int);
            finalMessage.append(std::to_string(i));
        } else if (*fmt == 'f') {
            double d = va_arg(args, double);
            finalMessage.append(std::to_string(d));
        } else if (*fmt == 's') {
            char *s = va_arg(args, char*);
            finalMessage += s;
        }
        ++fmt;
    }
    va_end(args);

    this->log("[INFO] " + finalMessage, 0);
}

/**
 * Logs event of type info [ERROR]
 * @param fmt char* of types "s" for string, "d" for int and "f" for double
 */
void Logger::error(char *fmt, ...) {
    std::string finalMessage = "";
    va_list args;
    va_start(args, fmt);
    while (*fmt != '\0') {
        if (*fmt == 'd') {
            int i = va_arg(args, int);
            finalMessage.append(std::to_string(i));
        } else if (*fmt == 'f') {
            double d = va_arg(args, double);
            finalMessage.append(std::to_string(d));
        } else if (*fmt == 's') {
            char *s = va_arg(args, char*);
            finalMessage += s;
        }
        ++fmt;
    }
    va_end(args);

    this->log("[ERROR] " + finalMessage, 1);
}

/**
 * Logs event of type info [DEBUG] only if DEBUG is true in Debug class
 * @param fmt char* of types "s" for string, "d" for int and "f" for double
 */
void Logger::debug(char *fmt, ...) {
    if (Debug::DEBUG) {
        std::string finalMessage = "";
        va_list args;
        va_start(args, fmt);
        while (*fmt != '\0') {
            if (*fmt == 'd') {
                int i = va_arg(args, int);
                finalMessage.append(std::to_string(i));
            } else if (*fmt == 'f') {
                double d = va_arg(args, double);
                finalMessage.append(std::to_string(d));
            } else if (*fmt == 's') {
                char *s = va_arg(args, char*);
                finalMessage += s;
            }
            ++fmt;
        }
        va_end(args);

        this->log("[DEBUG] " + finalMessage, 2);
    }
}

/**
 * Destructor
 */
Logger::~Logger() {
    this->logStream.close();
}
