//
// Created by Filip Jani on 24.10.16.
//

#ifndef UPS_SERVER_COMMUNICATION_H
#define UPS_SERVER_COMMUNICATION_H

#include <unistd.h>
#include <algorithm>
#include "ServerConfiguration.h"
#include "Util.h"
#include "Message.h"


class Communication {
public:
    /** Constructor */
    Communication(ServerConfiguration::configuration serverConfiguration);

    /** Read message from client */
    std::string receiveMessage(int socket);

    /** Sends message to client with socket */
    int sendMessage(int socket, std::string message);

    /** Checks if message is valid */
    bool isValidMessage(std::string message);

    void ackMessage(size_t hashCode);
    void addMessage(Message message);

private:
    /** Configuration of server */
    ServerConfiguration::configuration serverConfiguration;

    std::mutex messageMutex;

    static void messageSender(Communication * communication);

    std::shared_ptr<std::vector<Message>> m_messagesToSend;

    std::string m_message;
};


#endif //UPS_SERVER_COMMUNICATION_H
