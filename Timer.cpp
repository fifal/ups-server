//
// Created by Filip Jani on 8.11.16.
//
// Class providing basic timer function which is thread safe
//

#include <iostream>
#include "Timer.h"

/**
 * Starts a timer
 */
void Timer::start() {
    clock_gettime(CLOCK_MONOTONIC, &this->startTime);
}

/**
 * Gets how much time elapsed from the start of the timer
 * @return double elapsed time
 */
double Timer::elapsedTime() {
    clock_gettime(CLOCK_MONOTONIC, &this->finishTime);
    elapsed = (finishTime.tv_sec - startTime.tv_sec);
    elapsed += (finishTime.tv_nsec - startTime.tv_nsec) / 1000000000.0;
    return elapsed;
}