//
// Created by Filip Jani on 24.10.16.
//

#ifndef UPS_SERVER_LOGGER_H
#define UPS_SERVER_LOGGER_H


#include <string>
#include <fstream>
#include <iostream>
#include <mutex>
#include "stdarg.h"

class Logger {
private:
    /** Logging to console? */
    bool console;
    /** Logs message */
    void log(std::string message, int type);
    /** Gets current system time */
    std::string getTime();

    /** Loger variables */
    std::ofstream logStream;
    std::string logFile;
    std::mutex logMutex;

public:
    /** Constructor, console = bool -> logging into console */
    Logger(std::string filename, bool console);
    /** Destructor */
    ~Logger();

    /** Types of log event */
    void info(char* fmt, ...);
    void error(char *fmt, ...);
    void debug(char *fmt, ...);
};


#endif //UPS_SERVER_LOGGER_H
